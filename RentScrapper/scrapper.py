from requests import get
from bs4 import BeautifulSoup
from sys import argv
from DatabaseService import DatabaseService


def parse_price(price):
    return float(price.replace(' ', '').replace('zł', '').replace(',', '.'))


def room_number(nb):
    return float(nb.replace(' ', '').replace('pokoje', '').replace('pokoi', ''))


def parse_area(mkw):
    return float(mkw.replace(' ', '').replace('m²', ''))


URL = 'https://www.morizon.pl/do-wynajecia/mieszkania/wroclaw/?ps%5Bliving_area_from%5D=70&ps%5Bliving_area_to%5D=80&ps%5Bhas_balcony%5D=1'

databaseService = DatabaseService('rent_data.db')

if len(argv) > 1 and argv[1] == 'setup':
    databaseService.createRentOfferTable()
    quit()

page = get(URL)
bs = BeautifulSoup(page.content, 'html.parser')

for offer in bs.find_all('div', class_='single-result'):
    location = offer.find('h2', class_='single-result__title').get_text().strip()
    rent_per_month = parse_price(offer.find('p', class_='single-result__price').get_text().strip())
    date = offer.find('span', class_='single-result__category single-result__category--date').get_text().strip()
    link = offer.find('a')['href']
    description = offer.find('div', class_='description single-result__description').get_text().strip()
    param = offer.find_all('ul', class_='param list-unstyled list-inline')[0].find_all('li')
    rooms = room_number(param[0].get_text().strip())
    area_mkw = parse_area(param[1].get_text().strip())
    floor = param[2].get_text().strip()
    details = get(link)
    bs_details = BeautifulSoup(details.content, 'html.parser')
    amenities = bs_details.find('section', class_='propertyDetails').find_all('p')[0].get_text().strip()
    equipment = bs_details.find('section', class_='propertyDetails').find_all('p')[2].get_text().strip()

    databaseService.OfferTableAddValue(location, rent_per_month, area_mkw, rooms, floor, date, amenities, equipment, link, description)