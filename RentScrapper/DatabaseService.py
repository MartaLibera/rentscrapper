import sqlite3


class DatabaseService:
    def __init__(self, dbName):
        self.dbName = dbName
        self.db = sqlite3.connect(dbName)
        self.cursor = self.db.cursor()

    def __del__(self):
        print("closing db connection")
        self.db.close()

    def createRentOfferTable(self):
        self.cursor.execute(f'''CREATE TABLE 'scrapped_data' (location TEXT,
                                                        price REAL,
                                                        area REAL,
                                                        rooms REAL,
                                                        floor TEXT,
                                                        date TEXT,
                                                        amenities TEXT,
                                                        equipment TEXT,
                                                        link TEXT,
                                                        description TEXT)''')

    def OfferTableAddValue(self, *args):
        self.cursor.execute('''INSERT INTO 'scrapped_data' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''', (*args,))
        self.db.commit()
